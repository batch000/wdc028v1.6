//Sample
let sessionNumber = 35;

const { MongoClient } = require('mongodb');
const chai = require('chai');
const expect = chai.expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s35){

    try {

		// Requires the path to the index file inside the activity folder
		// const activity = require(`../backend/s${sessionNumber}/activity/index`);
		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);
        // console.log(activity)
        describe('s35', function() {

            let client;
            let db;
            let errors = [];
            let startTime;
            this.timeout(50000)
			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})

            before(async () => {
                startTime = performance.now();
                // Connect to local MongoDB server
                client = await MongoClient.connect('mongodb+srv://admin:admin1234@cluster0.zlyew.mongodb.net/bookingAPI?retryWrites=true&w=majority', { useUnifiedTopology: true });
                db = client.db('employees');
                db.users = db.collection('users')
                let res = await db.users.insertMany([
                    {
                        firstName: "Stephen",
                        lastName: "Hawking",
                        age: 76,
                        email: "stephenhawking@mail.com",
                        department: "HR"
                    },
                        {
                        firstName: "Niel",
                        lastName: "Armstrong",
                        age: 82,
                        email: "nielarmstrong@mail.com",
                        department: "HR"
                    },
                    {
                        firstName: "Bill",
                        lastName: "Gates",
                        age: 65,
                        email: "billgates@mail.com",
                        department: "Operations"
                    },
                    {
                        firstName: "John",
                        lastName: "Doe",
                        age: 21,
                        email: "johndoe@mail.com",
                        department: "HR"
                    }
                ]);

            });

            it('Find users with letter s in their first name or d in their last name', async () => {
                // Retrieve a users document with the specified name
                
                let users = await activity.findName(db).then(result => (result.toArray()));
                
                users.forEach((user) => {

                expect(user.firstName).to.satisfy(function () {
                    if ((user.firstName === 'Stephen') || (user.firstName === 'John')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found firstName property is not Stephen or John");

                expect(user.lastName).to.satisfy(function () {
                    if ((user.lastName === 'Hawking') || (user.lastName === 'Doe')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found lastName property is not Hawking or Doe");

                expect(user.age).to.satisfy(function () {
                    if ((user.age === 76) || (user.age === 21)) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found age property is not 76 or 21");

                expect(user.email).to.satisfy(function () {
                    if ((user.email === 'stephenhawking@mail.com') || (user.email === 'johndoe@mail.com')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found email property is not stephenhawking@mail.com or johndoe@mail.com");

                expect(user.department).to.satisfy(function () {
                    if (user.department === 'HR')  {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found department property is not HR");

                });
                
            });


            it('Find users who are from the HR department and their age is greater than or equal to 70.', async () => {
                // Retrieve a users document with the specified name
                
                let users = await activity.findDeptAge(db).then(result => (result.toArray()));
            
                users.forEach((user) => {

                expect(users).to.exist;

                expect(user.firstName).to.satisfy(function () {
                    if ((user.firstName === 'Stephen') || (user.firstName === 'Niel')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found firstName property is not Stephen or Niel");

                expect(user.lastName).to.satisfy(function () {
                    if ((user.lastName === 'Hawking') || (user.lastName === 'Armstrong')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found lastName property is not Hawking or Armstrong");

                expect(user.age).to.satisfy(function () {
                    if ((user.age === 76) || (user.age === 82)) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found age property is not 76 or 82");

                expect(user.email).to.satisfy(function () {
                    if ((user.email === 'stephenhawking@mail.com') || (user.email === 'nielarmstrong@mail.com')) {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found email property is not stephenhawking@mail.com or nielarmstrong@mail.com");

                expect(user.department).to.satisfy(function () {
                    if (user.department === 'HR')  {
                        return true;
                    } else {
                        return false;
                    }
                },"Users found department property is not HR");

                });
                
            });


            it('Find users with the letter e in their last name and has an age of less than or equal to 30.', async () => {

                // Retrieve a users document with the specified name
                let users = await activity.findNameAge(db).then(result => (result.toArray()));
                
                users.forEach((user) => {

                expect(user,"user cannot be found").to.exist;
                expect(user.firstName).to.equal('John',"firstName property is not John");
                expect(user.lastName).to.equal('Doe',"lastName property is not Doe");
                expect(user.age).to.equal(21,"age property is not 21");
                expect(user.email).to.equal('johndoe@mail.com',"email property is not johndoe@mail.com");
                expect(user.department).to.equal('HR',"Department property is not HR");

                })
                
            });
        
 
        after(async () => {

            const endTime = performance.now();
            const totalTime = (endTime - startTime) / 1000; // Convert to seconds
            console.log(`Total time taken: ${totalTime} seconds`);

            global.gradesObject[`s${sessionNumber}`] = {
                grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
                score: `${passingTests}/${totalTests}`,
                feedback: errors.length ? errors : "No Errors"
            }

            //Drop Database after use
            await db.dropDatabase()

            // Disconnect from MongoDB server
            await client.close();

            jsonData.gradedTest.s35 = true;
            await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
        });


});

        
} catch (err) {
    if(err.code === "MODULE_NOT_FOUND"){
      console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
    } else {
      console.log(err)
    }
}
} else {
    console.log(`S${sessionNumber} - Tested`);
}


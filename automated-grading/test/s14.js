// let sessionNumber = 14;
// require('geckodriver');
// // Generated by Selenium IDE
// const { Builder, By, Key, until } = require('selenium-webdriver')
// const firefox = require('selenium-webdriver/firefox');
// const { performance } = require('perf_hooks');
// const fs = require('fs');
// const path = require('path');
// const chai = require('chai');
// const assert = chai.assert;
// const expect = chai.expect;

// const data = fs.readFileSync('./test/grading.json');
// const jsonData = JSON.parse(data);

// if(!jsonData.gradedTest.s14){

// 	try {

//     if (!fs.existsSync('./sessions/frontend/s14/activity/index.html')) {
// 			throw new Error('File not found');
// 		  }

//       //Automated Grading Integration
//       // Variable to store passing unit test scores for the session
//       let passingTests = 0;
//       // Variable to store total number of session unit tests
//       let totalTests = 0;


//       function parsePixels(value) {
//           return parseFloat(value.replace('px', ''));
//         }

//       describe('s14', function() {

//         this.timeout(30000);
//         const timeout = 10000;
//         let driver;
//         let vars;

//         let internalCssTestPassed = false;

//         let startTime;

//         before(async function() {
//           startTime = performance.now();
//           // Any setup code you need to run before the tests start
//           driver = await new Builder().forBrowser('firefox').setFirefoxOptions(options).build()
//         });
        
//         const options = new firefox.Options();
//         options.headless();
  
//         options.setPreference('browser.download.manager.showWhenStarting', false);
//         options.setPreference('browser.download.manager.useWindow', false);
  
//         beforeEach(async function() {

//           totalTests += 1;
//           vars = {}
//         })
  
//         afterEach(function(){
  
//           if(this.currentTest.state === "passed"){
//             passingTests += 1;
//           }
  
//           process.removeAllListeners(); 
  
//         })

//         it('Element navbar is at the top and has display flex', async function() {

//             const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//             const url = `file://${filePath}`;
//             await driver.get(url);

//             const navbarElement = await driver.findElement(By.id('navbar'));

//             // Check if element is at the top of the page
//             const navbarRect = await navbarElement.getRect();
//             assert.isBelow(navbarRect.y, 10, 'Navbar should be at top of page');
        
//             // Check if element has display flex style
//             const navbarDisplayStyle = await navbarElement.getCssValue('display');
//             assert.strictEqual(navbarDisplayStyle, 'flex', 'Navbar should have display flex');

//         });

//         it('should have brand at left side of navbar', async function () {

//             const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//             const url = `file://${filePath}`;
//             await driver.get(url);

//             const navbarElement = await driver.findElement(By.id('navbar'));
//             const brandElement = await navbarElement.findElement(By.id('brand'));
        
//             // Check if brand element is on the left side of the navbar
//             const navbarRect = await navbarElement.getRect();
//             const brandRect = await brandElement.getRect();
//             const brandLeft = brandRect.x - navbarRect.x;
//             const brandWidth = brandRect.width;
//             expect(brandLeft).to.be.lessThan(brandWidth);
//         });

//         it('navlinks should be flex container', async function () {

//             const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//             const url = `file://${filePath}`;
//             await driver.get(url);
            
//             const navLinks = await driver.findElement(By.id('navlinks'));
        
//             // Check if brand element is on the left side of the navbar
//             const navlinksDisplay = await navLinks.getCssValue("display");
//             expect(navlinksDisplay).to.equal("flex");
//         });

//         it('main should be container-fluid and has padding', async function () {

//           const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//           const url = `file://${filePath}`;
//           await driver.get(url);
          
//           const main = await driver.findElement(By.id('main'));
      
//           // Check if brand element is on the left side of the navbar
//           const mainClass = await main.getAttribute('class');
//           expect(mainClass).to.include("container-fluid","div main is not container-fluid");
//           expect(mainClass).to.match(/p-[12345]/,"div main does not have instructed padding");
//       });

//         it('other-projects should be flex', async function () {

//             const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//             const url = `file://${filePath}`;
//             await driver.get(url);
            
//             const otherProj = await driver.findElement(By.id('other-projects'));
        
//             // Check if brand element is on the left side of the navbar
//             const otherProjStyle = await otherProj.getCssValue("display");
//             expect(otherProjStyle).to.include("flex");
//         });

//         it('other-projects should have justify-content-around', async function () {

//           const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//           const url = `file://${filePath}`;
//           await driver.get(url);
          
//           const otherProj = await driver.findElement(By.id('other-projects'));
      
//           // Check if brand element is on the left side of the navbar
//           const otherProjStyle = await otherProj.getCssValue("justify-content");
//           expect(otherProjStyle).to.include("space-around");
//         });

//         it('other-projects should have flex-row-reverse', async function () {

//           const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//           const url = `file://${filePath}`;
//           await driver.get(url);
          
//           const otherProj = await driver.findElement(By.id('other-projects'));
      
//           // Check if brand element is on the left side of the navbar
//           const otherProjStyle = await otherProj.getCssValue("flex-direction");
//           expect(otherProjStyle).to.include("row-reverse");
//         });

//         it('other-projects child divs should be col-md-4', async function () {

//           const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//           const url = `file://${filePath}`;
//           await driver.get(url);
          
//           const otherProjDivs = await driver.findElements(By.css('#other-projects > div'));
      
//           // Check if brand element is on the left side of the navbar
//           for(let i = 0; i < otherProjDivs.length; i++){
//             const otherProjClass = await otherProjDivs[i].getAttribute("class");
//             expect(otherProjClass).to.include("col-md-3");
//           }
//       });

//       it('should check if the core-tools div is using flex and flex-column', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("core-tools"));
//         const parentDirection = await parentElement.getCssValue("flex-direction");
//         const parentFlex = await parentElement.getCssValue("display");
//         expect(parentFlex).to.equal("flex","core-tools div is not using flex.");
//         expect(parentDirection).to.equal("column","core-tools div is not flex-column.");
//       });


//       it('should check if the html div image is centered using flex', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("html"));
//         const parentJustify = await parentElement.getCssValue("justify-content");
//         const parentFlex = await parentElement.getCssValue("display");
//         expect(parentFlex).to.equal("flex","html div is not using flex.");
//         expect(parentJustify).to.equal("center","html div children is not centered horizontally using flex.");
//       });

//       it('should check if the css-js div images is centered using flex', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("css-js"));
//         const parentFlex = await parentElement.getCssValue("display");
//         const parentJustify = await parentElement.getCssValue("justify-content");

//         expect(parentFlex).to.equal("flex","css-js div is not using flex.");
//         expect(parentJustify).to.equal("center","css-js div children is not centered horizontally using flex.");
//       });

//       it('should check if the other-tools div is using flex and flex-column', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("other-tools"));
//         const parentDirection = await parentElement.getCssValue("flex-direction");
//         const parentFlex = await parentElement.getCssValue("display");
//         const parentBreakPt = await parentElement.getAttribute("class");
        
//         expect(parentFlex).to.equal("flex","core-tools div is not using flex.");
//         expect(parentDirection).to.equal("column","core-tools div is not flex-column.");
//         expect(parentBreakPt).to.include("flex-md-column","core-tools div flex-column does not contain breakpoint.");
//       });

//       it('should check if the tools-1 div images is centered using flex', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("tools-1"));
//         const parentFlex = await parentElement.getCssValue("display");
//         const parentJustify = await parentElement.getCssValue("justify-content");

//         expect(parentFlex).to.equal("flex","css-js div is not using flex.");
//         expect(parentJustify).to.equal("center","css-js div children is not centered horizontally using flex.");
//       });

//       it('should check if the tools-2 div images is centered using flex', async function () {

//         // Navigate to the URL
//         const filePath = path.join(__dirname, '..','sessions','frontend','s14','activity','index.html');
//         const url = `file://${filePath}`;
//         await driver.get(url);

//         // Find the parent element by ID
//         const parentElement = await driver.findElement(By.id("tools-2"));
//         const parentFlex = await parentElement.getCssValue("display");
//         const parentJustify = await parentElement.getCssValue("justify-content");

//         expect(parentFlex).to.equal("flex","css-js div is not using flex.");
//         expect(parentJustify).to.equal("center","css-js div children is not centered horizontally using flex.");
//       });

//         after(async function () {          
//           const endTime = performance.now();
//           const totalTime = (endTime - startTime) / 1000; // Convert to seconds
//           console.log(`Total time taken: ${totalTime} seconds`);
          
          
//           global.gradesObject["s14"] = {
//             grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
//             score: `${passingTests}/${totalTests}`
//           }
          
//           jsonData.gradedTest.s14 = true;
//           await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
//           // Wait for all promises to settle before calling driver.quit()
//           // await Promise.all(globalThis.PromiseManager.promises);
//           await driver.quit(); 
//         });

//       })	

	
//     } catch (err) {
//       if(err.code === "MODULE_NOT_FOUND" || err.message === "File not found"){
//         console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
//       } else {
//         console.log(err)
//       }
//     }
// } else {
// 	console.log("s14 - Tested");
// }
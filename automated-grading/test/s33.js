//Sample
let sessionNumber = 33;

const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');



if(!jsonData.gradedTest.s33){

    try {

		// Requires the path to the index file inside the activity folder
		// const activity = require(`../backend/s${sessionNumber}/activity/index`);
		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		let user;
		let order;
		let product;
		let orderProduct;

		const userData = fs.readFileSync('./sessions/backend/s33/activity/user.json');
		user = JSON.parse(userData);
		const orderData = fs.readFileSync('./sessions/backend/s33/activity/order.json');
		order = JSON.parse(orderData);
		const productData = fs.readFileSync('./sessions/backend/s33/activity/product.json');
		product = JSON.parse(productData);
		const orderProductData = fs.readFileSync('./sessions/backend/s33/activity/orderProduct.json');
		orderProduct = JSON.parse(orderProductData);

		console.log(user,order,product,orderProduct);

		describe(`s${sessionNumber}`, function () {

            let startTime;
            let errors = [];
			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})

			before(() => {
			startTime = performance.now();
			// Read sample JSON document from file
			});

			it('should have the correct keys for user.json', () => {
			const dataModelKeys = [
				'id',
				'firstName',
				'lastName',
				'email',
				'password',
				'isAdmin',
				'mobileNo'
			];

			const documentKeys = Object.keys(user);

			expect(documentKeys).to.have.members(dataModelKeys,"A property is missing/not included from user.json");
			});

			
			it('should have the correct data types for user.json', () => {
			expect(user.id).to.be.a('string',"id property is not string type.");
			expect(user.firstName).to.be.a('string',"firstName property is not string type.");
			expect(user.lastName).to.be.a('string',"lastName property is not string type.");
			expect(user.email).to.be.a('string',"email property is not string type.");
			expect(user.password).to.be.a('string',"password property is not string type.");
			expect(user.isAdmin).to.be.a('boolean',"isAdmin property is not boolean type.");
			expect(user.mobileNo).to.be.a('string',"mobileNo property is not string type.");
			});


			it('should have the correct keys for order.json', () => {
			const dataModelKeys = [
			"id",
			"userId",
			"transactionDate",
			"status",
			"total"
			];

			const documentKeys = Object.keys(order);

			expect(documentKeys).to.have.members(dataModelKeys,"A property is missing/not included from order.json");
			});


			it('should have the correct data types for order.json', () => {
			expect(order.id).to.be.a('string',"order id property is not string type.");
			expect(order.userId).to.be.a('string',"userID property is not string type.");
			expect(order.transactionDate).to.be.a('string',"transactionDate property is not string type.");
			expect(order.status).to.be.a('string',"status property is not string type.");
			expect(order.total).to.be.a('number',"total property is not number type.");
			});

			it('should have the correct keys for product.json', () => {
			const dataModelKeys = [
			"id",
			"name",
			"description",
			"price",
			"stocks",
			"isActive",
			"sku"
			];

			const documentKeys = Object.keys(product);

			expect(documentKeys).to.have.members(dataModelKeys,"A property is missing/not included from product.json");
			});


			it('should have the correct data types for product.json', () => {
			expect(product.id).to.be.a('string',"product id property is not string type.");
			expect(product.name).to.be.a('string',"name property is not string type.");
			expect(product.description).to.be.a('string',"description property is not string type.");
			expect(product.price).to.be.a('number',"price property is not number type.");
			expect(product.stocks).to.be.a('number',"stocks property is not number type.");
			expect(product.isActive).to.be.a('boolean',"isActive property is not boolean type.");
			expect(product.sku).to.be.a('string',"sku property is not string type.");
			});

			it('should have the correct keys for orderProduct.json', () => {
			const dataModelKeys = [
			"orderId",
			"productId",
			"quantity",
			"price",
			"subTotal"
			];

			const documentKeys = Object.keys(orderProduct);

			expect(documentKeys).to.have.members(dataModelKeys),"A property is missing/not included from orderProduct.json";
			});


			it('should have the correct data types for orderProduct.json', () => {
			expect(orderProduct.orderId).to.be.a('string',"orderId property is not string type.");
			expect(orderProduct.productId).to.be.a('string',"productId property is not string type.");
			expect(orderProduct.quantity).to.be.a('number',"quantity property is not number type.");
			expect(orderProduct.price).to.be.a('number',"price property is not number type.");
			expect(orderProduct.subTotal).to.be.a('number',"subTotal id property is not number type.");
			});


			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
				
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s33 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});

        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND" || err.code === "ENOENT"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
    console.log(`S${sessionNumber} - Tested`);
}




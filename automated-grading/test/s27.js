// Variable to store session number for printing out the grade for the session
let sessionNumber = 27;

const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s27){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {
            let {registeredTrainers,registeredPokemon} = activity;

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})

            // Unit tests for the activity
            it('should add a new value inside the registeredPokemon array', () => {
                activity.addPokemon("Charmander");
                expect(registeredPokemon).to.include("Charmander","addPokemon() method cannot add a new pokemon in the registeredPokemons array");
            });

            it('should delete a value inside the registeredPokemon array', () => {
                activity.deletePokemon();
                expect(registeredPokemon.length).to.equal(0,"deletePokemon() method cannot delete a pokemon in the registeredPokemons array");
                expect(activity.deletePokemon().toLowerCase()).to.include("no","deletePokemon() does not return a proper string when trying to delete item in empty registeredPokemons array");
            });

            it('should return the length registeredPokemon array', () => { 
                expect(activity.displayNumberOfPokemons().toLowerCase()).to.include("no","displayNumberOfPokemons() does not return a proper string when trying to check length of empty registeredPokemons array");
                activity.addPokemon("Charmander");
                expect(activity.displayNumberOfPokemons()).to.equal(1,"displayNumberOfPokemons() method does not return the length of registeredPokemons array");
            });

            it('should sort registeredPokemon array', () => { 
                activity.deletePokemon();
                expect(activity.sortPokemon().toLowerCase()).to.include("no","sortPokemon() does not return a proper string when trying to sort empty registeredPokemons array");
                activity.addPokemon("Charmander");
                activity.addPokemon("Articuno");
                activity.sortPokemon();
                expect(registeredPokemon[0]).to.equal("Articuno","sortPokemon() method does not sort registeredPokemons array");
            });

            it('should be able to add a new trainer object in the registeredTrainers array', () => { 
                activity.registerTrainer("Satoshi",15,["Pikachu","Metapod","Bulbasaur"]);
                expect(registeredTrainers.length).to.equal(1,"registerTrainer() method does not add a trainer in registeredTrainers array");
                expect(typeof registeredTrainers[0]).to.equal("object","registerTrainer() does not push an object in registeredTrainers array");
                expect(typeof registeredTrainers[0].trainerName).to.equal("string"," trainerName property is not a string.");
                expect(typeof registeredTrainers[0].trainerLevel).to.equal("number"," trainerName property is not a number.");
                expect(registeredTrainers[0].pokemons).to.be.an.instanceof(Array,"pokemons property is not an array")

            });

            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(async function () {
                
                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

                jsonData.gradedTest.s27 = true;
                await fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
        
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}
} else {
    console.log(`S${sessionNumber} - Tested`);
}



// Variable to store session number for printing out the grade for the session
let sessionNumber = 25;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

const { performance } = require('perf_hooks');

if(!jsonData.gradedTest.s25){

    try {

        // Requires the path to the index file inside the activity folder
        const activity = require(`../sessions/backend/s${sessionNumber}/activity/index`);

        // Variable to store passing unit test scores for the session
        let passingTests = 0;
        // Variable to store total number of session unit tests
        let totalTests = 0;

        describe(`s${sessionNumber}`, function () {

            let startTime;
            let errors = [];
			before(async function() {
			  startTime = performance.now();
              originalConsoleLog = console.log;
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

                if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})


            // Unit tests for the activity
            // fix
            it('should have the right properties within the object and talk function should log to console', () => {
                expect(activity.trainer).to.have.property('name')
                expect(activity.trainer).to.have.property('age')
                expect(activity.trainer).to.have.property('pokemon')
                expect(activity.trainer).to.have.property('friends')
                expect(activity.trainer).to.have.property('talk')

                activity.trainer.talk()
                expect(activity.trainer.talk()).to.include('choose')
            })

            // fix
            it('should be able to create a new Pokemon', () => {
                let pikachu = new activity.Pokemon('Pikachu', 12)

                expect(pikachu).to.have.property('name')
                expect(pikachu).to.have.property('level')
                expect(pikachu).to.have.property('health')
                expect(pikachu).to.have.property('attack')
                expect(pikachu).to.have.property('tackle')
                expect(pikachu).to.have.property('faint')
            })
            
            // fix
            it('should contain tackle and faint functions', () => {

                let pikachu = new activity.Pokemon('Pikachu', 12)
                let geodude = new activity.Pokemon('Geodude', 15)

                expect(geodude.tackle(pikachu)).to.include("reduced")
                
                expect(pikachu.faint()).to.include("fainted")

            })

            // Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
            // Use of objects with the "session number" as keys to allow for ease of automation
            after(function () {

                console.log = originalConsoleLog;
                const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);
                
				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}
                
                jsonData.gradedTest.s25 = true;
                fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
            });

        });
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
    console.log(`S${sessionNumber} - Tested`);
}




// Variable to store session number for printing out the grade for the session
let sessionNumber = 19;

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const { performance } = require('perf_hooks');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s19){

	try {
		// Requires the path to the index file inside the activity folder
		const activity = require(`../backend/sessions/s${sessionNumber}/activity/index`);


		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;



		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})

			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				// If there are any errors, save them to the array
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
				
			})


			// Unit tests for the activity
			it(`firstName should have a string data type`, function () {
				expect(activity.firstName).to.be.a('string',"firstName variable is not a string.");
			});

			it(`lastName should have a string data type`, function () {
				expect(activity.lastName).to.be.a('string',"lastName variable is not a string.");
			});

			it(`age should have a number data type`, function () {
				expect(activity.age).to.be.a('number',"number variable is not a number.");
			});

			it(`hobbies should have an array data type`, function () {
				expect(activity.hobbies,"Hobbies is not an array").to.be.instanceof(Array);
			});

			it(`workAddress should be an object data type`, function () {
				expect(activity.workAddress).to.be.an('object',"workAddress is not an object.");
			});

			it('fullName should be a string', function () {
				expect(activity.fullName).to.be.a('string',"fullName is not a string.");
			});

			it('yearOld should be a number data type', function () {
				expect(activity.currentAge).to.be.a('number',"fullName is not a number.");
			});

			it('friends should be an array data type' , function () {
				expect(activity.friends,"friends is not an array").to.be.instanceof(Array);
			});

			it('profile should be an object data type', function () {
				expect(activity.profile).to.be.an('object',"profile is not an object.");
			});

			it(`The 'fullName2' variable should be a string`, function () {
				expect(activity.fullName2).to.be.a('string',"fullName2 is not a string.");		
			});

			it(`The variable 'location' should be a string`, function () {
				expect(activity.lastLocation).to.be.a('string',"lastLocation is not a string.");		
			});

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}


				jsonData.gradedTest.s19 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
			console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
			console.log(err)
		}
	}
} else {
	console.log("S15 - Tested");
}

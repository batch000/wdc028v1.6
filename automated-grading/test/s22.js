// Variable to store session number for printing out the grade for the session
let sessionNumber = 22;
const { performance } = require('perf_hooks');

const assert = require('assert'); 
const expect = require("chai").expect;
const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s22){

	try {

		// Requires the path to the index file inside the activity folder
		const activity = require(`../backend/sessions/s${sessionNumber}/activity/index`);


		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;


		describe(`s${sessionNumber}`, function () {
			
			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})


			// Unit tests for the activity
			it(`The result should be equal to the sum of the two parameters`, function () {
				
				expect(activity.addNum(15,5)).to.equal(20);

			});

			it(`The result should be equal to the difference of the two parameters`, function () {
				
				expect(activity.subNum(20, 5)).to.equal(15);
			});	

			it(`The result should be equal to the product of the two parameters`, function () {
				expect(activity.multiplyNum(50, 10)).equal(500);
			});

			it(`The result should be equal to the quotient of the two parameters`, function () {
				expect(activity.divideNum(50, 10)).equal(5);
			});

			it(`The result should be able to get total area of a circle from a provided radius`, function () {
				expect(activity.getCircleArea(15)).equal(706.86);
			});

			it(`should be equal to the average of 20,40,60 and 80`, function () {
				expect(activity.getAverage(20,40,60,80)).equal(50);
			});

			it(`should show if 38/50 is a passing score`, function () {
				expect(activity.checkIfPassed(38,50)).equal(true);
			});


			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s22 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {
	console.log(`S${sessionNumber} - Tested`);
}


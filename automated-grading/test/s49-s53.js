// const chai = require('chai');
// // Chai http package allows for use of request method to process HTTP requests on API
// const http = require('chai-http');
// // Stores expect method from chai module to use for sending HTTP requests
// const expect = chai.expect;
// // Allows use of chai methods like "request", "post", etc.
// chai.use(http);

// const jwt = require("jsonwebtoken");

// const fs = require('fs');

// const data = fs.readFileSync('./test/grading.json');
// const jsonData = JSON.parse(data);

// if(!jsonData.capstone2.isTested){

// 	try {

// 		// Variable to store passing unit test scores for the session
// 		let passingTests = 0;
// 		// Variable to store total number of session unit tests
// 		let totalTests = 0;
			

// 		describe(`s49-s53`, function () {

// 			this.timeout(30000);

// 			// Increments the totalTests variable to use in computation of session score
// 			beforeEach(function () {
// 				totalTests += 1;
// 			})

// 			let itemId;

// 			it("test_api_register_user", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post('/users/register')
// 				.type('json')
// 				.send({
// 					email: "sample2@mail.com",
//                     password: "password1234"
// 				})
// 				.end((err, res) => {
//                     console.log(res.body);
// 					expect(res.body).to.equal(true);
// 					done();

// 				})
// 			})


//             it("test_api_login_user", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post('/users/login')
// 				.type('json')
// 				.send({
// 					email: "sample@mail.com",
//                     password: "password1234"
// 				})
// 				.end((err, res) => {

                   

//                    const isVerified = jwt.verify(res.body.access, "capstone2", (err, data) => {
//                         if (err) {
//                             return false
//                         } else {
            
//                             return true
            
//                         }
//                     })

//                     expect(res.body.access,"Token does not exist").to.exist;
//                     expect(isVerified).to.equal(true,"Token is not legitimate");
//                     done()

// 				})
// 			})

// 			it("test_api_get_user_details", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post('/users/login')
// 				.type('json')
// 				.send({
// 					email: "sample@mail.com",
//                     password: "password1234"
// 				})
// 				.end((err, res) => {

// 					let userToken = res.body.access
                
//                    chai.request(jsonData.capstone2.api)
// 				   .post(`/users/details`)
// 				   .set('authorization', `Bearer ${userToken}`)
// 				   .end((err,res)=>{
// 						expect(res.body).to.exist;
// 						done();
// 				   })

// 				})
// 			})

//             it("test_api_set_user_as_admin", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
//                     password: "admin1234"
// 				})
// 				.end((err, res) => {      

// 					expect(res.body).to.not.equal(false,"Login cannot be completed.");

//                    const adminToken = res.body.access;

//                     chai.request(jsonData.capstone2.api)
//                     .post(`/users/login`)
//                     .type('json')
//                     .send({
// 						email: "sample2@mail.com",
// 						password: "password1234"
//                     })
//                     .end((err, res) => {
                           
//                        const decode = jwt.verify(res.body.access, "capstone2", (err, data) => {
//                             if (err) {
//                                 return false
//                             } else {
                
//                                 return data
                
//                             }
//                         })

// 						expect(decode).to.not.equal(false,"JWT Token is unverifiable");
    
//                         const userId = decode.id;

//                         chai.request(jsonData.capstone2.api)
//                         .put(`/users/${userId}/setAsAdmin`)
//                         .set('authorization', `Bearer ${adminToken}`)
//                         .end((err, res) => {

// 							expect(res.body).to.not.equal(false,"Something went wrong when setting as admin.");

//                             chai.request(jsonData.capstone2.api)
//                             .post(`/users/login`)
//                             .type('json')
//                             .send({
// 								email: "sample2@mail.com",
// 								password: "password1234"
//                             })
//                             .end((err, res) => {
                                                   
//                                const decode = jwt.verify(res.body.access, "capstone2", (err, data) => {
//                                     if (err) {
//                                         return false
//                                     } else {
                        
//                                         return data
                        
//                                     }
//                                 })
            
//                                 expect(decode.isAdmin).to.equal(true);
//                                 done();
        
//                             })
        
//                         })
    
//                     })

// 				})
// 			})

// 			it("test_api_get_all_items", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.get(`/products/all`)
// 				.type('json')
// 				.end((err, res) => {                  

// 					expect(res.body.length).to.be.above(0,"get all items route cannot retrieve all items.");
// 					done();

// 				})
// 			})

// 			it("test_api_get_all_active_items", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.get(`/products/active`)
// 				.type('json')
// 				.end((err, res) => {                  
// 					expect(res.body.length).to.be.above(0,"get all active items route cannot retrieve all active items.");
// 					expect(res.body.every(product => product.isActive === true)).to.equal(true,"not all items retrieved are active");
// 					done();
// 				})
// 			})

// 			it("test_api_admin_only_create_items", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
//                     password: "admin1234"
// 				})
// 				.end((err, res) => {

// 					let adminToken = res.body.access;
					
// 					chai.request(jsonData.capstone2.api)
// 					.post(`/products/`)
// 					.set('authorization', `Bearer ${adminToken}`)
// 					.type('json')
// 					.send({
// 						name: "newItem",
// 						description: "Item created for checking.",
// 						price: 1500
// 					})
// 					.end((err,res)=>{

// 						expect(res.body).to.equal(true,"Item creation cannot be completed.");

// 					})

// 					chai.request(jsonData.capstone2.api)
// 					.post(`/users/login`)
// 					.type('json')
// 					.send({
// 						email: "sample@mail.com",
// 						password: "password1234"
// 					})
// 					.end((err, res) => {
	
// 						let newToken = res.body.access;
						
// 						chai.request(jsonData.capstone2.api)
// 						.post(`/products/`)
// 						.set('authorization', `Bearer ${newToken}`)
// 						.type('json')
// 						.send({
// 							name: "newItem",
// 							description: "Item created for checking.",
// 							price: 1500
// 						})
// 						.end((err,res)=>{
	
// 							expect(res.body).to.equal(false,"Item creation has no admin restrictions");
// 							done();
	
// 						})
	
	
// 					})   


// 				})     


// 			})

			
// 			it("test_api_get_single_item", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.get(`/products/all`)
// 				.type('json')
// 				.end((err, res) => {                  
// 					let itemId = res.body[0]._id;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/products/${itemId}`)
// 					.type('json')
// 					.end((err, res) => {     

// 						expect(res.body,"Cannot get single item").to.exist;					
// 						done();
// 					})


// 				})
// 			})


// 			it("test_api_admin_only_update_item", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
//                     password: "admin1234"
// 				})
// 				.end((err, res) => {

// 					let adminToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/products/all`)
// 					.type('json')
// 					.end((err, res) => {  

// 						res.body.reverse();

// 						let itemId = res.body[0]._id;

// 						chai.request(jsonData.capstone2.api)
// 						.put(`/products/${itemId}`)
// 						.set('authorization', `Bearer ${adminToken}`)
// 						.type('json')
// 						.send({
// 							name: "sampleItem",
// 							description: "New Item Updated",
// 							price: 2500
// 						})
// 						.end((err,res)=>{
	
// 							expect(res.body).to.equal(true,"Item update cannot be completed.");
	
// 						})
	
// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "sample@mail.com",
// 							password: "password1234"
// 						})
// 						.end((err, res) => {
		
// 							let newToken = res.body.access;
							
// 							chai.request(jsonData.capstone2.api)
// 							.put(`/products/${itemId}`)
// 							.set('authorization', `Bearer ${newToken}`)
// 							.type('json')
// 							.send({
// 								name: "newItem",
// 								description: "Item updated for checking.",
// 								price: 1500
// 							})
// 							.end((err,res)=>{
		
// 								expect(res.body).to.equal(false,"Item update has no admin restrictions");
// 								done();
		
// 							})
		
		
// 						})   
	
	
// 					})
					
// 				})     

// 			})

// 			it("test_api_admin_only_archive_item", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
//                     password: "admin1234"
// 				})
// 				.end((err, res) => {

// 					let adminToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/products/all`)
// 					.type('json')
// 					.end((err, res) => {  

// 						res.body.reverse();

// 						let itemId = res.body[0]._id;

// 						chai.request(jsonData.capstone2.api)
// 						.put(`/products/${itemId}/archive`)
// 						.set('authorization', `Bearer ${adminToken}`)
// 						.end((err,res)=>{
	
// 							expect(res.body).to.equal(true,"Item archive cannot be completed.");

// 							chai.request(jsonData.capstone2.api)
// 							.get(`/products/${itemId}`)
// 							.type('json')
// 							.end((err, res) => {     
		
// 								expect(res.body.isActive).to.equal(false,"Item not archived.");			
// 							})
	
// 						})
	
// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "sample@mail.com",
// 							password: "password1234"
// 						})
// 						.end((err, res) => {
		
// 							let newToken = res.body.access;
							
// 							chai.request(jsonData.capstone2.api)
// 							.put(`/products/${itemId}/archive`)
// 							.set('authorization', `Bearer ${newToken}`)
// 							.end((err,res)=>{
		
// 								expect(res.body).to.equal(false,"Item update has no admin restrictions");
// 								done();
		
// 							})
		
		
// 						})   
	
	
// 					})
					
// 				})     

// 			})


// 			it("test_api_admin_only_activate_item", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
//                     password: "admin1234"
// 				})
// 				.end((err, res) => {

// 					let adminToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/products/all`)
// 					.type('json')
// 					.end((err, res) => {  

// 						res.body.reverse();

// 						let itemId = res.body[0]._id;

// 						chai.request(jsonData.capstone2.api)
// 						.put(`/products/${itemId}/activate`)
// 						.set('authorization', `Bearer ${adminToken}`)
// 						.end((err,res)=>{
	
// 							expect(res.body).to.equal(true,"Item activate cannot be completed.");

// 							chai.request(jsonData.capstone2.api)
// 							.get(`/products/${itemId}`)
// 							.type('json')
// 							.end((err, res) => {     
		
// 								expect(res.body.isActive).to.equal(true,"Item not activated.");			
// 							})
	
// 						})
	
// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "sample@mail.com",
// 							password: "password1234"
// 						})
// 						.end((err, res) => {
		
// 							let newToken = res.body.access;
							
// 							chai.request(jsonData.capstone2.api)
// 							.put(`/products/${itemId}/activate`)
// 							.set('authorization', `Bearer ${newToken}`)
// 							.end((err,res)=>{
		
// 								expect(res.body).to.equal(false,"Item update has no admin restrictions");
// 								done();
		
// 							})
		
		
// 						})   
	
	
// 					})
					
// 				})     

// 			})

// 			it("test_api_user_only_checkout", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "sample@mail.com",
// 					password: "password1234"
// 				})
// 				.end((err, res) => {

// 					let userToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/products/active`)
// 					.end((err,res)=>{

// 						let item1 = res.body[0];


// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/checkout`)
// 						.set('authorization', `Bearer ${userToken}`)
// 						.type('json')
// 						.send({
// 							products : [
// 								{
// 									productId: item1._id,
// 									quantity : 2
// 								}
// 							],
// 							totalAmount : 3000
// 						})
// 						.end((err, res) => {  
	
// 							expect(res.body).to.equal(true);
		
// 						})

// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "admin@mail.com",
// 							password: "admin1234"
// 						})
// 						.end((err, res) => {

// 							let adminToken = res.body.access;

// 							chai.request(jsonData.capstone2.api)
// 							.post(`/users/checkout`)
// 							.set('authorization', `Bearer ${adminToken}`)
// 							.type('json')
// 							.send({
// 								products : [
// 									{
// 										productId: item1._id,
// 										quantity : 2
// 									}
// 								],
// 								totalAmount : 3000
// 							})
// 							.end((err, res) => {  
		
// 								expect(res.body).to.equal(false,"Checkout is not user only.");
// 								done();
			
			
// 							})


// 						})


// 					})


					
// 				})     

// 			})


// 			it("test_api_user_only_orders", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "sample@mail.com",
// 					password: "password1234"
// 				})
// 				.end((err, res) => {

// 					let userToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/users/myOrders`)
// 					.set('authorization', `Bearer ${userToken}`)
// 					.type('json')
// 					.end((err,res)=>{

// 						expect(res.body.length).to.be.above(0,"Cannot access user's orders");

// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "admin@mail.com",
// 							password: "admin1234"
// 						})
// 						.end((err, result) => {

// 							let adminToken = result.body.access;

// 							chai.request(jsonData.capstone2.api)
// 							.get(`/users/myOrders`)
// 							.set('authorization', `Bearer ${adminToken}`)
// 							.type('json')
// 							.end((err, res) => {  
		
// 								expect(res.body).to.equal(false,"Admin can access orders.");
// 								done();
			
			
// 							})


// 						})


// 					})


					
// 				})     

// 			})

// 			it("test_api_admin_only_allOrders", (done) => {

// 				chai.request(jsonData.capstone2.api)
// 				.post(`/users/login`)
// 				.type('json')
// 				.send({
// 					email: "admin@mail.com",
// 					password: "admin1234"
// 				})
// 				.end((err, res) => {

// 					let adminToken = res.body.access;

// 					chai.request(jsonData.capstone2.api)
// 					.get(`/users/orders`)
// 					.set('authorization', `Bearer ${adminToken}`)
// 					.type('json')
// 					.end((err,res)=>{

// 						expect(res.body.length).to.be.above(0,"Cannot access all user's orders");

// 						chai.request(jsonData.capstone2.api)
// 						.post(`/users/login`)
// 						.type('json')
// 						.send({
// 							email: "sample@mail.com",
// 							password: "password1234"
// 						})
// 						.end((err, result) => {

// 							let userToken = result.body.access;

// 							chai.request(jsonData.capstone2.api)
// 							.get(`/users/orders`)
// 							.set('authorization', `Bearer ${userToken}`)
// 							.type('json')
// 							.end((err, res) => {  
		
// 								expect(res.body).to.equal(false,"User can access all orders.");
// 								done();
			
			
// 							})


// 						})


// 					})
					
// 				})     

// 			})



// 			// Increases the amount of passing tests if test succeeds to generate final score
// 			afterEach(function () {
// 				// this.currentTest.state allows to access the state of each test after it is run
// 				// returns either "passed" or "failed" for the "state" property
// 				if (this.currentTest.state == "passed") {
// 					passingTests += 1;
// 				}
// 			})

// 			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
// 			// Use of objects with the "session number" as keys to allow for ease of automation
// 			after(function () {
// 				global.gradesObject["s49-s53"] = {
// 					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
// 					score: `${passingTests}/${totalTests}`
// 				}

// 				jsonData.capstone2.isTested = true;
// 				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
// 			});

// 		});
		
// 	} catch (err){
// 		console.log(`Capstone cannot be found.`);
// 	}

// } else {
// 	console.log(`S42-S46 - Tested`);
// }



// Variable to store session number for printing out the grade for the session
let sessionNumber = 20;

const { performance } = require('perf_hooks');
const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);


if(!jsonData.gradedTest.s20){


	try {

		// Requires the path to the index file inside the activity folder
		const activity = require(`../backend/sessions/s${sessionNumber}/activity/index`);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;

		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				// If there are any errors, save them to the array
				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})

			// Unit tests for the activity
			it(`The result of num1 + num2 should be 30.`, function () {
				expect(activity.num1 + activity.num2).equal(30);
			});

			it(`The result of num3 + num4 should be 200.`, function () {
				expect(activity.num3 + activity.num4).equal(200);
			});

			it(`The result of num5 - num6 should be 7`, function () {
				expect(activity.num5 - activity.num6).equal(7);
			});

			it(`resultMinutes should be equal to 525600 `, function () {	
				expect(activity.resultMinutes).equal(525600);	
				
			});

			it(`resultFahrenheit should be equal to 269.6`, function () {
				expect(activity.resultFahrenheit).equal(269.6);
			});

			it(`remainder1 should be equal to 5`, function () {
				expect(activity.remainder1).equal(5);
			});

			it(`isDivisibleBy8 should be false`, function () {
				expect(activity.isDivisibleBy8).equal(false);
			});

			it(`remainder2 should be equal to 0`, function () {
				expect(activity.remainder2).equal(0);
			});

			it(`isDivisibleBy4 should be true`, function () {
				expect(activity.isDivisibleBy4).equal(true);
			});

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);


				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}

				jsonData.gradedTest.s20 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));

			});

		});

	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}

} else {

	console.log("S16 - Tested");

}




// Variable to store session number for printing out the grade for the session
let sessionNumber = 21;
const { performance } = require('perf_hooks');
const assert = require('assert'); 
const expect = require("chai").expect;

const fs = require('fs');

const data = fs.readFileSync('./test/grading.json');
const jsonData = JSON.parse(data);

if(!jsonData.gradedTest.s21) {

	try {
		// Requires the path to the index file inside the activity folder
		const activity = require(`../backend/sessions/s${sessionNumber}/activity/index`);

		// Variable to store passing unit test scores for the session
		let passingTests = 0;
		// Variable to store total number of session unit tests
		let totalTests = 0;


		describe(`s${sessionNumber}`, function () {

			let startTime;
			let errors = [];
			before(async function() {
			  startTime = performance.now();
			});

			// Increments the totalTests variable to use in computation of session score
			beforeEach(function () {
				totalTests += 1;
			})
			afterEach(function () {
				// this.currentTest.state allows to access the state of each test after it is run
				// returns either "passed" or "failed" for the "state" property
				if (this.currentTest.state == "passed") {
					passingTests += 1;
				}

				if (this.currentTest.state === 'failed') {
					errors.push({
						test: this.currentTest.title,
						feedback: this.currentTest.err.message
					});
				}
			})


			// Unit tests for the activity
			it(`getUserInfo() is able to return on object with the correct properties.`, function () {


				expect(activity.getUserInfo(),"returned object has no name property.").to.have.property('name')
                expect(activity.getUserInfo(),"returned object has no age property.").to.have.property('age')
                expect(activity.getUserInfo(),"returned object has no address property.").to.have.property('address')
                expect(activity.getUserInfo(),"returned object has no isMarried property.").to.have.property('isMarried')
                expect(activity.getUserInfo(),"returned object has no petName property.").to.have.property('petName')

			});

			it(`getArtistsArray() is able to return an array of 5 favorite bands/musical artists as strings..`, function () {
				
				expect(activity.getArtistsArray().length).to.be.at.least(5,"returned array does not have at least 5 items");

				expect(activity.getArtistsArray().every(artist => typeof artist === "string"),"not all items in returned array are strings").to.be.ok;

			});

			it(`getSongsArray() is able to return an array of 5 favorite songs as strings.`, function () {
				
				expect(activity.getSongsArray().length).to.be.at.least(5,"returned array does not have at least 5 items");

				expect(activity.getSongsArray().every(song => typeof song === "string"),"not all items in returned array are strings").to.be.ok;

			});

			it(`getMoviesArray() is able to return an array of 5 favorite movies as strings.`, function () {
				
				expect(activity.getMoviesArray().length).to.be.at.least(5,"returned array does not have at least 5 items");

				expect(activity.getMoviesArray().every(movie => typeof movie === "string"),"not all items in returned array are strings").to.be.ok;

			})

			it(`getPrimeNumberArray() is able to return an array of 5 prime numbers`, function () {
				
				expect(activity.getPrimeNumberArray().length).to.be.at.least(5,"returned array does not have at least 5 items");

				expect(activity.getPrimeNumberArray().every(number => {

					let isPrime = true;

					if (number === 1) {
						return false
					} else if (number > 1) {

						for (let i = 2; i < number; i++) {
							if (number % i == 0) {
								isPrime = false;
								break;
							}
						}
					
						if (isPrime) {
							return true
						} else {
							return false
						}
					} else {
						return false
					}
					
				}),"One of the given numbers is not a prime number.").to.be.ok;

			})

			// Updates the "gradesObject" global variable to store the "grade equivalent" and "score count" to be printed as text for readability
			// Use of objects with the "session number" as keys to allow for ease of automation
			after(function () {

				
				const endTime = performance.now();
				const totalTime = (endTime - startTime) / 1000; // Convert to seconds
				console.log(`Total time taken: ${totalTime} seconds`);

				global.gradesObject[`s${sessionNumber}`] = {
					grade: `${passingTests/totalTests >= 0.75 ? "P" : "F"}`,
					score: `${passingTests}/${totalTests}`,
					feedback: errors.length ? errors : "No Errors"
				}
				jsonData.gradedTest.s21 = true;
				fs.writeFileSync('./test/grading.json', JSON.stringify(jsonData,null,4));
			});

		});
		
	} catch (err) {
		if(err.code === "MODULE_NOT_FOUND"){
		  console.error(`s${sessionNumber} cannot be found. Check activity folder.`);
		} else {
		  console.log(err)
		}
	}


} else {

	console.log("S17 - Tested");

}


